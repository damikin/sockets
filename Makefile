DAYTIME_CD = daytime-client
DAYTIME_SD = daytime-server
DAYTIME_C = $(DAYTIME_CD)/daytime-client
DAYTIME_S = $(DAYTIME_SD)/daytime-server
DAYTIME_A = $(DAYTIME_C) $(DAYTIME_S)

ECHO_CD = echo-client
ECHO_SD = echo-server
ECHO_C = $(ECHO_CD)/echo-client
ECHO_S = $(ECHO_SD)/echo-server
ECHO_A = $(ECHO_C) $(ECHO_S)

all: $(DAYTIME_A) $(ECHO_A)

$(DAYTIME_A):
	@-echo '#######################'
	$(MAKE) -C $(DAYTIME_SD)
	@-echo '#######################'
	$(MAKE) -C $(DAYTIME_CD)
	@-echo '#######################'

$(ECHO_A):
	@-echo '#######################'
	$(MAKE) -C $(ECHO_SD)
	@-echo '#######################'
	$(MAKE) -C $(ECHO_CD)
	@-echo '#######################'

clean:
	@-echo '#######################'
	$(MAKE) -C $(DAYTIME_CD) clean
	@-echo '#######################'
	$(MAKE) -C $(DAYTIME_SD) clean
	@-echo '#######################'
	@-echo '#######################'
	$(MAKE) -C $(ECHO_CD) clean
	@-echo '#######################'
	$(MAKE) -C $(ECHO_SD) clean
	@-echo '#######################'

