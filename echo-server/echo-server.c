#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "helpers.h"

void do_stuff(int sockfd);

int
main(void) {
	int listenfd, connfd;
	char buf[MAXLINE + 1];
	socklen_t len;
	struct sockaddr_in servaddr;
	struct sockaddr_in cliaddr;

	if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		my_error("socket error");

	memset(&servaddr, 0, sizeof servaddr);
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(ECHO_PORT); /* my echo port */

	if(bind(listenfd, (struct sockaddr *) &servaddr, sizeof servaddr) < 0)
		my_error("bind error");

	if(listen(listenfd, 1024) < 0)
		my_error("listen error");

	print_name(listenfd);

	for(;;) {
		len = sizeof cliaddr;
		connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len);
		if(connfd < 0)
			my_error("accept error");

		/* fork to child and do everything */
		if(fork()== 0) {
			if(close(listenfd) < 0)
				fprintf(stderr, "child: error closing listenfd: %s\n", strerror(errno));

			printf("child: connection from: %s:%d\n",
				inet_ntop(AF_INET, &cliaddr.sin_addr, buf, MAXLINE),
				ntohs(cliaddr.sin_port));

			do_stuff(connfd);

			printf("child: close: %s:%d\n",
				inet_ntop(AF_INET, &cliaddr.sin_addr, buf, MAXLINE),
				ntohs(cliaddr.sin_port));

			if(close(connfd) < 0)
				my_error("child: close error");

			exit(EXIT_SUCCESS);
		}

		if(close(connfd) < 0)
			my_error("close error");
	}

	exit(EXIT_SUCCESS);
}

void
do_stuff(int sockfd) {
	ssize_t n;
	char buf[MAXLINE];

again:
	while( (n = read(sockfd, buf, MAXLINE)) > 0)
		if(write(sockfd, buf, n) < 0)
			my_error("child: write error");

	if(n < 0) {
		if(errno == EINTR)
			goto again;
		else
			my_error("child: read error");
	}
}

