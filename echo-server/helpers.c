#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "helpers.h"

void
my_error(const char *str) {
	fprintf(stderr, "%s: %s\n", str, strerror(errno));
	exit(EXIT_FAILURE);
}

void
print_name(int sockfd) {
	struct sockaddr_in ss;
	socklen_t len;
	char buff[MAXLINE];

	len = sizeof ss;
	if(getsockname(sockfd, (struct sockaddr *) &ss, &len) < 0)
		my_error("getsockname error");

	puts("IPv4");
	printf("server info: %s:%d\n",
		inet_ntop(AF_INET, &ss.sin_addr, buff, MAXLINE),
		ntohs(ss.sin_port));
}

