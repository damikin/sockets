#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "helpers.h"

void do_stuff(FILE *fp, int sockfd);

int
main(int argc, char *argv[]) {
	int sockfd;
	struct sockaddr_in servaddr;

	if(argc != 2) {
		fprintf(stderr, "usage: %s IP\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		my_error("socket error");

	memset(&servaddr, 0, sizeof servaddr);
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT_ECHO); /* my echo port */

	if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)
		my_error("inet_pton error");

	if(connect(sockfd, (struct sockaddr *) &servaddr, sizeof servaddr) < 0)
		my_error("connect error");

	do_stuff(stdin, sockfd);

	if(close(sockfd) < 0)
		my_error("close error");

	exit(EXIT_SUCCESS);
}

void
do_stuff(FILE *fp, int sockfd) {
	char sendline[MAXLINE], recvline[MAXLINE];
	ssize_t ret;

	while(fgets(sendline, MAXLINE, fp)) {
again:
		if( (ret = write(sockfd, sendline, strlen(sendline))) < 0) {
			if(errno == EINTR)
				goto again;
			else
				my_error("write error");
		}

		if(readline(sockfd, recvline, MAXLINE) == 0)
			my_error("server terminated prematurely");

		if(fputs(recvline, stdout) == EOF)
			my_error("puts error");
	}
}

