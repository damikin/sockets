#ifndef HELPERS_H_
#define HELPERS_H_

#define MAXLINE  4 * 1024

#define PORT_DAYTIME 38013
#define PORT_ECHO    38877

void my_error(const char *str);
void print_name(int sockfd);
size_t readline(int sockfd, char *buf, size_t size);

#endif

