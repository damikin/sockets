#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "helpers.h"

int
main(void) {
	int listenfd, connfd;
	time_t ticks;
	char buff[MAXLINE + 1];
	socklen_t len;
	struct sockaddr_in servaddr;
	struct sockaddr_in cliaddr;

	if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		my_error("socket error");

	memset(&servaddr, 0, sizeof servaddr);
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(PORT_DAYTIME); /* my daytime server */

	if(bind(listenfd, (struct sockaddr *) &servaddr, sizeof servaddr) < 0)
		my_error("bind error");

	if(listen(listenfd, 1024) < 0)
		my_error("listen error");

	print_name(listenfd);

	for(;;) {
		len = sizeof cliaddr;
		connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len);
		if(connfd < 0)
			my_error("accept error");

		/* fork to child and do everything */
		if(fork()== 0) {
			if(close(listenfd) < 0)
				fprintf(stderr, "child: error closing listenfd: %s\n", strerror(errno));

			printf("connection from: %s:%d\n",
				inet_ntop(AF_INET, &cliaddr.sin_addr, buff, MAXLINE),
				ntohs(cliaddr.sin_port));

			ticks = time(NULL);
			snprintf(buff, sizeof buff, "%.24s\r\n", ctime(&ticks));
			if(write(connfd, buff, strlen(buff)) < 0)
				my_error("write error");

			if(close(connfd) < 0)
				my_error("child: close error");

			exit(EXIT_SUCCESS);
		}

		if(close(connfd) < 0)
			my_error("close error");
	}

	exit(EXIT_SUCCESS);
}

