#ifndef HELPERS_H_
#define HELPERS_H_

#define MAXLINE  4 * 1024

#define PORT_DAYTIME 38013
#define ECHO_PORT    38877

void my_error(const char *str);
void print_name(int sockfd);

#endif

