#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "helpers.h"

int
main(int argc, char *argv[]) {
	int sockfd;
	ssize_t n;
	char line[MAXLINE + 1];
	struct sockaddr_in servaddr;

	if(argc != 2) {
		fprintf(stderr, "usage: %s IP\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		my_error("socket error");

	memset(&servaddr, 0, sizeof servaddr);
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT_DAYTIME); /* my daytime server */

	if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)
		my_error("inet_pton error");

	if(connect(sockfd, (struct sockaddr *) &servaddr, sizeof servaddr) < 0)
		my_error("connect error");

	while( (n = read(sockfd, line, MAXLINE)) > 0) {
		line[n] = '\0'; /* NULL terminate */
		if(fputs(line, stdout) == EOF)
			my_error("fputs error");
	}

	if(n < 0)
		my_error("read error");

	exit(EXIT_SUCCESS);
}

